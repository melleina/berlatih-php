<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>
<body>
    <?php
    function ubah_huruf($string){
        $string1 = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", 
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
        $newstring = "";

        foreach(str_split($string) as $string2){
            for($i = 0; $i < count($string1); $i++){
                if (strtolower($string2) == $string1[$i]){
                    $newstring .= $string1[$i+1];
                }
            }
        }
        return $newstring ."<br>";
            
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>
</html>